from datetime import datetime

from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

from carshowrooms.mixins import IsArchivedMixin


class Showroom(IsArchivedMixin):
    name = models.CharField(max_length=50)
    balance = models.FloatField(validators=[MinValueValidator(0)])
    latitude = models.FloatField(validators=[MinValueValidator(-90), MaxValueValidator(90)])
    longitude = models.FloatField(validators=[MinValueValidator(-180), MaxValueValidator(180)])

    def __str__(self):
        return f'{self.name} - {self.balance}; {self.latitude} - {self.longitude}. {self.is_archived}'


class Supplier(IsArchivedMixin):
    title = models.CharField(max_length=50)
    creation_date = models.DateTimeField(default=datetime.now)

    def __str__(self):
        return f"{self.title} - {self.creation_date}. {self.is_archived}"


class Customer(IsArchivedMixin):
    name = models.CharField(max_length=50)
    balance = models.FloatField(validators=[MinValueValidator(0)])

    def __str__(self):
        return f"{self.name} - {self.balance}. {self.is_archived}"


class Moderator(IsArchivedMixin):
    pass

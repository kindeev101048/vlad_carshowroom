from datetime import datetime

from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

from carshowrooms.mixins import IsArchivedMixin


class Car(IsArchivedMixin):
    mark = models.CharField(max_length=20)
    model = models.CharField(max_length=20)
    price = models.FloatField(max_length=20, validators=[MinValueValidator(0)])
    is_on_sale = models.BooleanField(default=False)
    sale = models.ForeignKey('Sale', on_delete=models.CASCADE, related_name='cars')

    def __str__(self):
        return f"{self.mark} - {self.price} - {self.is_on_sale}. {self.is_archived}"


class Offer(IsArchivedMixin):
    price = models.FloatField(validators=[MinValueValidator(0)])
    creation_date = models.DateTimeField(default=datetime.now)
    car = models.ForeignKey(Car, on_delete=models.DO_NOTHING, related_name='offers')
    showroom = models.ForeignKey('accounts.Showroom', on_delete=models.DO_NOTHING, related_name='offers')
    supplier = models.ForeignKey('accounts.Supplier', on_delete=models.DO_NOTHING, related_name='offers', null=True)
    customer = models.ForeignKey('accounts.Customer', on_delete=models.DO_NOTHING, related_name='offers', null=True)

    def __str__(self):
        return f"{self.price} - {self.creation_date} - {self.car.name}. {self.is_archived}"


class Sale(IsArchivedMixin):
    percentage = models.FloatField(validators=[MinValueValidator(0), MaxValueValidator(90)])

    def __str__(self):
        return f"{self.percentage}. {self.is_archived}"

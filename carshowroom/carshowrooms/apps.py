from django.apps import AppConfig


class CarshowroomsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'carshowrooms'

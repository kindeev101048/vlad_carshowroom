# Use the official Python base image
FROM python:3.11.8-slim
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
# Set the working directory
WORKDIR /app

# Install dependencies
COPY requirements.txt .

RUN python -m pip install --upgrade pip
RUN pip install -U pip && pip install -r ./requirements.txt

# Copy the application code
COPY carshowroom .

# Expose the port the app will run on
EXPOSE 8080
